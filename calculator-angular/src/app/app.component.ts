import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
  display = '0';
  firstValue = 1;
  action: string | null = null;

  numClick(val: number) {
    if (this.display === '0') {
      this.display = val.toString();
    } else {
      this.display = `${this.display}${val}`;
    }
  }

  oper(action: any) {
    this.firstValue = parseFloat(this.display);
    this.action = action;
    this.display = '';
  }

  calculate() {
    const a = this.firstValue;;
    const b = parseFloat(this.display);
    let result;

    if (this.action === 'x') {
      result = a * b;
    }
    else if (this.action === '/') {
      result = a / b;
    }
    else if (this.action === '+') {
      result = a + b;
    }
    else if (this.action === '-')
      result = a - b;
    else {
      result = 999;
    }

    this.display = result.toString();
  }
}
